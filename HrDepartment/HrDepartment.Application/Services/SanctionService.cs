﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HrDepartment.Application.Interfaces;
using HrDepartment.Domain.Entities;

namespace HrDepartment.Application.Services
{
	public class SanctionService : ISanctionService
	{
		private readonly List<JobSeeker> sanctionList = new List<JobSeeker>();
		public Task<bool> IsInSanctionsListAsync(string lastName, string firstName, string middleName, DateTime birthDate)
		{
			var result = sanctionList.Exists(x => 
			x.LastName == lastName && x.FirstName == firstName && x.MiddleName == middleName && x.BirthDate == birthDate);
			return Task.FromResult(result);
		}
		public void Add(JobSeeker jobSeeker)
		{
			sanctionList.Add(jobSeeker);
		}
	}
}
