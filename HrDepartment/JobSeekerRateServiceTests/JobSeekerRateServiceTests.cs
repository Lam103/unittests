using NUnit.Framework;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using Moq;
using HrDepartment.Application.Interfaces;
using System;
using HrDepartment.Domain.Enums;

namespace JobSeekerRateServiceTests
{

    public class JobSeekerRateServiceTests
    {

        private JobSeeker _jobSeeker = new JobSeeker();
        private SanctionService _sanctionServise = new();

        [Test]
        public void Then_JobSeekerRateService_null_return_null_exception()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(null));
        }

        [TestCase(10, BadHabits.None)]
        [TestCase(5, BadHabits.Smoking)]
        [TestCase(-5, BadHabits.Alcoholism)]
        [TestCase(-35, BadHabits.Drugs)]
        public void CalculateHabitsRating_Return_int_rating(int expected, BadHabits bedHabits)
        {
            //Arrange
            var result = new JobSeekerRateService(_sanctionServise);
            //Act
            _jobSeeker.BadHabits = bedHabits;
            var actual = result.CalculateHabitsRating(_jobSeeker.BadHabits);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(0.75, 5)]
        [TestCase(1.5, 10)]
        [TestCase(3.5, 15)]
        [TestCase(7, 25)]
        [TestCase(20, 40)]
        public void CalculateExperienceRating_return_int_rating(double experience, int expected)
        {
            //Arrange
            var res = new JobSeekerRateService(_sanctionServise);
            //Act
            var actual = res.CalculateExperienceRating(experience);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(0, EducationLevel.None)]
        [TestCase(5, EducationLevel.School)]
        [TestCase(15, EducationLevel.College)]
        [TestCase(35, EducationLevel.University)]
        public void CalculateEducationRating_Return_int_rating(int expected, EducationLevel education)
        {
            //Arrange
            var result = new JobSeekerRateService(_sanctionServise);
            //Act
            _jobSeeker.Education = education;
            var actual = result.CalculateEducationRating(_jobSeeker.Education);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(-17, 0)]
        [TestCase(-66, 0)]
        [TestCase(-35, 10)]
        public void CalculateBirthDateRating_Return_int_rating(int age, int expected)
        {
            //Arrange
            
            var res = new JobSeekerRateService(_sanctionServise);
            //Act
            _jobSeeker.BirthDate = DateTime.Now.AddYears(age);
            var actual = res.CalculateBirthDateRating(_jobSeeker.BirthDate);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(-25,5.4, BadHabits.Smoking, EducationLevel.College,55)]//10+25+5+15
        public void CalculateJobSeekerRatingAsync_Return_int_rating(int age, double experience, BadHabits bedHabits, EducationLevel education, int expected)
        {
            //Arrange
             var res = new JobSeekerRateService(_sanctionServise);
            //Act
            
            _jobSeeker.BirthDate = DateTime.Now.AddYears(age);
            _jobSeeker.BadHabits = bedHabits;
            _jobSeeker.Experience = experience;
            _jobSeeker.Education = education;
            _sanctionServise.IsInSanctionsListAsync("lName", "fName", "mName", _jobSeeker.BirthDate);
            var actual = res.CalculateJobSeekerRatingAsync(_jobSeeker).Result;
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestCase(-25, 5.4, BadHabits.Smoking, EducationLevel.College, 0)]//10+25+5+15
        public void CalculateJobSeekerRatingAsync_Return_rating_0(int age, double experience, BadHabits bedHabits, EducationLevel education, int expected)
        {
            //Arrange
            _jobSeeker.BirthDate = DateTime.Now.AddYears(age);
            _jobSeeker.BadHabits = bedHabits;
            _jobSeeker.Experience = experience;
            _jobSeeker.Education = education;
            _jobSeeker.FirstName = "FirstName";
            _jobSeeker.LastName = "LastName";
            _jobSeeker.MiddleName = "MiddleName";
            _sanctionServise.Add(_jobSeeker);
            var res = new JobSeekerRateService(_sanctionServise);
            //Act
            var actual = res.CalculateJobSeekerRatingAsync(_jobSeeker).Result;
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}